<?php
  require  'database.php';
$sql = "SELECT * FROM `uploads`";
$result = mysqli_query($db,$sql);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Image Upload</title>
    <style type="text/css">
        #content{
            width: 50%;
            margin: 20px auto;
            border: 1px solid #cbcbcb;
        }
        form{
            width: 50%;
            margin: 20px auto;
        }
        form div{
            margin-top: 5px;
        }
        #img_div{
            width: 80%;
            padding: 5px;
            margin: 15px auto;
            border: 1px solid #cbcbcb;
        }
        #img_div:after{
            content: "";
            display: block;
            clear: both;
        }
        img{
            float: left;
            margin: 5px;
            width: 300px;
            height: 140px;
        }
        table{
            margin: 0 auto;
            border: 2px solid black;
        }
        table, tr, td {
            padding: 10px;
            text-align: center;
            border: 1px solid black;
            border-collapse: collapse;
        }
        .time {
            background-color: #848484;
            padding: 10px;
            overflow: hidden;
        }
        p {
            color: white;
            padding: 5px;
            text-align: center;
            display: inline-block;
            /*float: top;*/
        }
        p:nth-child(2) {
            float: right;
        }

    </style>
</head>
<body>
<div id="content">

    <div class="time">
        <p>Image Upload in PHP</p>
        <p><?php
            date_default_timezone_set('Asia/Dhaka');

            echo 'Time : '.date('h:i:sa');

            ?></p>
    </div>

    <form method="POST" action="database.php" enctype="multipart/form-data">
        <input type="hidden" name="size" value="1000000">
        <div>
            <input type="file" name="image">
        </div>
        <div>
      <textarea
          id="text"
          cols="40"
          rows="4"
          name="image_text"
          placeholder="Say something about this image..."></textarea>
        </div>
        <div>
            <button type="submit" name="upload">POST</button>
        </div>
    </form>

    <table>
        <tr>
            <td>Id</td>
            <td>Image</td>
            <td>Text</td>
        </tr>

        <?php
    while ($row = mysqli_fetch_array($result)) { ?>
            <tr>
                <td></td>
                <td><img src='images/<?=$row['image']?>' style="width: 80px; height: 80px;"></td>
                <td><?=$row['image_text']?></td>
            </tr>

        <?php }?>
    </table>
    <br>
</div>
</body>
</html>